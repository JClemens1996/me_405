'''@file Lab2script.py'''
# @author Joshua Clemens
# @copyright Cal Poly SLO, ME 405
# @date May 6th, 2020

# @package encoder
# The encoder is intended to record the angular displacement of the motor using
# a quadrature encoder

# @author Joshua Clemens
# @copyright Cal Poly SLO, ME 405
# @date May 6th, 2020
    
import pyb

#this line imports the pyb script to access the library contained on the PCB

class Encoder:
    '''This class implements encoder functionality into the ME 405 board. This is important because it is going to be used
    to measure the position of the motor in a unit corresponding to fraction of a revolution. This can and will be used to
    implement position control in the motor, but first it is important that the encoder can read the motor position, store
    it in a boundless variable, and account for underflow and overflow error since the encoder timer has a range limited by
    a 16 bit unsigned number.'''
    
    ##Defines Encoder class
    
    # @Details Enables quadrature encoder readouts according to a
    # corresponding pinset and timer channel combination
    
    def __init__(self, ENC_A, ENC_B, Timer_Num):
        
        '''Creates encoder object by initializing PCB pinsets and enabling
        the proper timer in encoder counter mode.
        @param ENC_A The pyb.pin object associated with encoder phase A
        @param ENC_B The pyb.pin object associated with encoder phase B
        @param timer The pyb.pin object associated with the timer channel'''
        
        #convert class arguments to attributes
        
        self._ENC_A=ENC_A
        self._ENC_B=ENC_B
        self._Timer_Num=Timer_Num
        
        #Initialization message
        
        print ('Encoder object is being created')
        
        #Initialize timer
        self._tim_ob=pyb.Timer(Timer_Num)
        self._tim_ob.init(period=65535,prescaler=0)
        
        #initialize timer channels
        
        self._tch1=self._tim_ob.channel(1,pyb.Timer.ENC_AB,pin=ENC_A)
        self._tch2=self._tim_ob.channel(2,pyb.Timer.ENC_AB,pin=ENC_B)
        
        #create class variables and clear them by default
        
        ## rec_position stores the recorded 16 bit unsigned number position of the motor as read by the encoder
        #
        #  rec_position is a 16 bit unsigned number because it ranges from 0-65535. This is the value used to actually
        #  calculate delta because it will account for underflow or overflow in the timer counter.
        self.rec_position=self._tim_ob.counter()
        
        ## delta stores the change in position from the last stored encoder reading in rec_position
        #
        # delta is meant to store the change in position from the encoder. This serves the dual purpose function of
        # knowing which way the motor is turning, and adding up these changes to account for the motors current position
        # at any instant
        self.delta=self._tim_ob.counter()-self.rec_position
        
        ## tot_position stores the current position of the motor as referenced by some arbitrary datum
        #
        # tot_position gets its name from the fact that it is the result of totaling up all the delta values of time. In
        # a way it measures the total change in position of the motor, but only if the reference datum is 0. Furthermore, it
        # would only measure net change in position.
        self.tot_position=0
        
        #initialization complete message
        #prompt users which pinset and which timer was initialized 
        
        print ('Encoder object has been created for the following pinset/timer combination:')
        print ('Encoder phase A: ' +str(self._ENC_A))
        print ('Encoder phase B: ' +str(self._ENC_B))
        print ('Using channels 1 & 2 of timer ' +str(self._Timer_Num))
        
        
    def update(self):
        
            '''This method updates the encoder to its new position based on the amount 
                of ticks'''
                
            #set recorded position to current encoder count in order to update
            #then print value to user for debugging purposes
            
            self.rec_position=self._tim_ob.counter()
            #print ('The updated motor position is ' +str(self.rec_position))
            
    def set_position(self,new_pos):
        
            '''This method sets the position to a specified value within the 
                16 bit range of valid values. Also clears total displacement 
                (tot_position) and current displacement (delta), since a new 
                arbitrary starting position has been declared. This method 
                requires the user to input the desired position as an 
                argument.
                @param new_pos The users desired position to set the motor to'''
                
            #change position in counter and then set recorded position equal to this
            
            self._tim_ob.counter(new_pos)  
            self.rec_position=new_pos
            
            #accordingly clear delta and net displacement to reflect new origin
            #display to user the new position that they set
            
            self.delta=0
            self.tot_position=0
            
            print ('The motor position has been set to ' +str(new_pos))
            
    def get_delta(self):
        
            '''This method calculates the difference between the last recorded 
                encoder reading and the current encoder reading. It returns 
                this delta value as the difference and keeps a running total
                of net displacement
                @return delta - Change in position of the motor from the last recorded position
                @return tot_position - The current position of the model, calculated by totaling up all the delta values'''
                
            #first calculate delta as current counter minus last recorded position
            
            self.delta=self._tim_ob.counter()-self.rec_position
            
            #Test for underflow error
            
            if (self.delta>=32768):
                
            #correct underflow error if it occurs
                
                
                self.delta=self.delta-65535
                
            #test for overflow error
                
            elif (self.delta<=-32768):
                
            #correct overflow error if it occurs
                
                self.delta=self.delta+65535
                
            #display the differential displacement
            
            if self.delta!=0:
                print('The difference in position is '+str(self.delta))
            
            #calculate net displacement
            
            self.tot_position=self.tot_position+self.delta
            self.rec_position=self._tim_ob.counter()
            
            
    def get_position(self):
        
            '''This method simply returns the net displacement value of the 
            motor WRT the arbitrarily declared start position. This origin 
            value is 0 be default, until manually changed by the user'''
            
            self.get_delta()
            #print the last recorded net displacement value
            
            print ('The current motor position is '+str(self.tot_position))
            
    def get_16bit_pos(self):
        
            '''This method returns the most recently updated position. Note:
                This may not be the physical current position'''
                
            #print the last recorded position. Weaker version of update method
            
            print ('The unsigned 16-bit motor position value is ' +str(self.rec_position))
            return self.rec_position
            
if __name__== '__main__':
   
    #The code below is meant to write a test program for the encoder class. It
    #only executes if the script is executed as a standalone program.  
    
    #create object for encoder 1
            
        ENC_1 = Encoder (pyb.Pin.board.PB6,pyb.Pin.board.PB7,4)
        
    #create object for encoder 2
        
        ENC_2 = Encoder (pyb.Pin.board.PC6,pyb.Pin.board.PC7,8) 
        
    #show users current encoder origin for reference
        
        ENC_1.get_position()
        ENC_2.get_position()
        import utime
        while True:
            ENC_1.get_position()
            utime.sleep(2)