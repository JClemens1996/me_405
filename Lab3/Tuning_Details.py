## @file Tuning_Details.py
# Brief doc for Tuning_Details.py
#
# Detailed doc for Tuning_Details.py
#
# I tuned my controller based on predetermined performance criteria. For example, I wanted to minimize percent overshoot
# so the target range was +/- 5% overshoot. Furthermore, I knew steady state error would be a potential problem since this controller
# only uses proportional control, so the target goal for this criteria was a steady state value within 1% margin of error from desired
# setpoint position. as the graph below shows, larger Kps (anywhere greater than 1) resulted in system instability with high percent
# overshoot and oscillatory behavior. In some cases, the system would never reach steady state because higher Kp's amplified the
# error signal too much such that the actuation signal caused too much overshoot and the 1% margin of error criteria was never satisfied.
# The plots below also shows that Kp's between than 0.1 and 0.5 approach a critically damped system with no overshoot.
# @image html Kp_5.JPG
# This image shows a step response with Kp = 5. The system shows a fast transient response, but with a significant amount of overshoot.
# Despite having no steady state error, this response is undesirable because of its high overshoot. A more damped system is desired.
# @image html Kp_05.JPG
# In order to combat the overshoot from the previous photo, a Kp = 0.5 was tried for the figure above. This results in a little bit less
# overshoot (but not by much), while introducing quite a bit of oscillatory behavior. This is undesirable because it prolongs the transient
# response and takes longer to reach steady state. Furthermore, for this system that type of behavior is not very problematic, but for other
# general purpose proportional controllers you may not want any overshoot.
# @image html Kp_01.JPG
# For this image, a Kp = 0.1 was tested. It can be seen that the transient takes a little bit longer, but since the time constant of this 1st
# order system model is in the range of milliseconds, the longer transient response is warranted by the fact that there is virtually no overshoot.
# Instead, this system represents a critically damped system because this Kp value was the smallest possible such that there was no overshoot.
# Similarly, this graph more accurately depicts the type of response expected from a step input applied to a first order system.
# @image html Kp_Overlayed.JPG
# This final image shows all three step responses overlaid on the same plot for comparison. They all arrive at steady state with no steady state
# error. The main distinguishing fact is how much overshoot is present and what the approximate settling time of the system is.