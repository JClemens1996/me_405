'''@file Lab3script.py'''
# @author Joshua Clemens
# @copyright Cal Poly SLO, ME 405
# @date May 12th, 2020

# @package P_controller
# The proportional controller calculates an error signal based on the difference between user input setpoint position and current motor position.
# This value is amplified (or attenuated) by a controller gain which is then sent as an actuation signal to the motor driver.

# @author Joshua Clemens
# @copyright Cal Poly SLO, ME 405
# @date May 12th, 2020

import pyb

class P_controller:
    
    '''This class implements proportional position control into the ME 405 board. This is accomplished by taking user
    input for a desired setpoint position along with a proportional gain value and calculating an actuation signal to
    send to the motor driver. Details on how this is done can be found in the update method of this class.'''
    
    ##Defines controller class
    
    # @Details Enables proportional control for a certain motor and encoder pair
    
    def __init__(self,motor_name,enc_name):
        
        '''Creates controller object by initializing the controller to work with a motor and encoder pair
        @param motor_name The general name of the motor as specified by the user
        @param enc_name The general name of the encoder as specified by the user'''
        
        self._motor_name=motor_name
        self._enc_name=enc_name
        while 2>1:
            try:
                self.Kp=float(input("What value of proportional control gain, Kp, would you like? "))
                if self.Kp <0:
                    print ("Negative numbers for proportional gain aren't allowed. Please input a positive value.")
                    continue
                print ("Value of " +str(self.Kp) +" stored successfully!")
                break
            except ValueError:
                print ("That's not a number")
                continue
            
            
        
        
        
    def update(self,setpoint,rec_pos):
        
        '''This method calculates an error signal based on the users desired input and current motor position
         @param setpoint The value that corresponds to the users desired motor position
         @param rec_pos The last known recorded position of the motor'''
        
        import utime
        self._position=['position']
        self._time=['time']
        
        ## Kp stores the proportional gain constant that either attenuates or amplifies the error signal into the actuation signal
        #
        #  Kp takes on very particular units depending on the system it is controlling. For purposes of our system, the stable
        # operating range for Kp lies between 0.1-10. Anything less than 0.1 and the system takes too long to respond and the program
        # will eventually crash after the system runs out of RAM because a desirable steady state will never be reached and the program
        # will run indefinitely. Anything greater than 10 and the system becomes unstable because the actuation signal fluctuates too
        # greatly for the system to ever reach steady state. Instead excessive oscillatory behavior is observed.
        self.Kp=self.Kp
        self._rec_pos=rec_pos
        
        ## Setpoint stores the desired motor position value that the user wants
        #
        # Setpoint is a value that tells the controller what position to move the motor to. It can contain any positive or negative whole
        # number integer, however it's worth noting that values greater than 75000 or less than -75000 will take the program so long to
        # reach steady-state that it may crash the program. The motor just can't move quick enough to arrive at setpoints that large
        # before the PC runs out of RAM. Setpoint is used in the calculation of error since error is the difference between current
        # position and desired position.
        self.setpoint= setpoint
        
        ## Error stores the difference between current motor position and desired motor position
        #
        # Error can be very large, well beyond the full duty cycle of the motor, since the motors range of position is technically
        # infinite. The program runs the controller until error is within a reasonable margin. For purposes of this system, that range
        # is approximately 1%.
        self.error=self.setpoint-self._rec_pos
        
        ## Act_signal (actuation signal) is simply the result of multiplying error by the proportional gain constant.
        #
        # Act_signal is the value that gets sent to the motor to set the duty cycle. One key point is that the actuation signal can either
        # be attenuated or amplified depending on what Kp value is used. Step response tuning shows what effect different Kp values had
        # on the system. Moreover, actuation signal can saturate the duty cycle range of the motor, but this is okay since the set_duty
        # method of the motor driver handles these values fine.
        self.act_signal=self.error*self.Kp
        while self.error < -10 or self.error >10:
            self._motor_name.set_duty(self.act_signal)
            self._enc_name.get_delta()
            self._rec_pos=self._enc_name.tot_position
            self.error=self.setpoint-self._rec_pos
            self.act_signal=self.error*self.Kp
            utime.sleep_ms(1)
            self._ticks=utime.ticks_ms()
            self._position.append(self._enc_name.tot_position)
            self._time.append(self._ticks)
        self._motor_name.set_duty(0)
            
            
            
        
        
    