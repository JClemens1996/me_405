'''@file main.py'''
# @author Joshua Clemens
# @copyright Cal Poly SLO, ME 405
# @date May 6th, 2020

# @package main
# Main imports the three modules used for position motor control. This consists of the proportional controller,
# encoder, and motor driver modules

# @author Joshua Clemens
# @copyright Cal Poly SLO, ME 405
# @date May 6th, 2020

# import all necessary modules
from motor import MotorDriver
from encoder import Encoder
from controller import P_controller

#import from .py library

import pyb
import utime

#user would declare which pinset and timer combination to be used for the motor

pin_EN = pyb.Pin (pyb.Pin.board.PA10, pyb.Pin.OUT_PP)
pin_IN1 = pyb.Pin (pyb.Pin.board.PB4, pyb.Pin.OUT_PP)
pin_IN2 = pyb.Pin (pyb.Pin.board.PB5, pyb.Pin.OUT_PP)
tim = pyb.Timer(3, freq=20000)
moe = MotorDriver(pin_EN, pin_IN1, pin_IN2, tim)

#user would declare which pinset and timer combination to be used for the encoder

ENC_1 = Encoder (pyb.Pin.board.PB6,pyb.Pin.board.PB7,4)

#infinite loop that asks user for desired setpoint

while True: 
    try:
        #ask user for integer setpoint
        setpoint=int(input("What integer position would you like to set the motor to? "))
        #error message otherwise
    except ValueError:
        print ("That's not a whole number")
        continue
    setpoint=int(setpoint)
    rec_pos=ENC_1.tot_position
    pcontrol1=P_controller(moe, ENC_1)
    pcontrol1.update(setpoint, rec_pos)
    print ("The desired position of " +str(setpoint) +" has been reached!")
    for pos_list,time_list in zip(pcontrol1._position,pcontrol1._time):
        print (pos_list,"," ,time_list)
    
        
    