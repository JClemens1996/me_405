'''@file main_IMU.py'''
import pyb
import utime
import ustruct
from pyb import I2C
from IMU_Controller import IMU
bus_addr=1
dev_addr=40
Imu_1= IMU(bus_addr,dev_addr)
mode=0b00001100
Imu_1.enable()
Imu_1.set_mode(0b00001100)

Imu_1.calib_stat()
while True:
    Imu_1.orientation()
    Imu_1.velocity()
    utime.sleep(2)
