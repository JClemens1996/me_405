## @page test IMU Driver Details
#
# @section sec_testing Testing Procedures
# The IMU was tested as shown in the calibration video by rotating at 45 degree increments about a single
# rotational axis. Once calibration was successful, magnetic north could be discerned by finding when the
# Euler angle had a complete vector readout of 0. Further testing was done to make sure that the Euler angles
# did in fact change by 90 degrees in the readout when the IMU was rotated by 90 degrees. Ensuring this meant
# the IMU position sensors were working as intended. The angular velocity readouts of the IMU was tested by
# ensuring that it had an approximate vector readout of 0 when stationary and sensible values of appropriate
# sign and magnitude when rotating the IMU in free space about any direction. The video below provides detail
# and proof into how this was done and how testing was successful.
# @section sec_response Response Video
# The video below shows a brief visualization of how the user-interface readouts change as the position of the
# IMU is changed. Key observations are pointed out in the video caption.
# @image html IMU_test.gif "IMU response feedback as position in space is changed. Notes detailed below:"
# Note how velocity readout is 0 when the IMU is stationary and position changes by approximately 90 degrees in the readout for a 90 degree change
# of position in space