'''@file IMU_controller.py'''
# @author Joshua Clemens
# @copyright Cal Poly SLO, ME 405
# @date May 20th, 2020

# @package IMU_controller
# The IMU determines position and velocity of the system using a magnetometer, accelerometer, and
# gyroscope as spatial coordinate sensors.

# @author Joshua Clemens
# @copyright Cal Poly SLO, ME 405
# @date May 20th, 2020

class IMU:
    
    '''The IMU class is responsible for using the 3 built in sensors of the IMU to generate
    the inertial/positional coordinates of the device. The accelerometer provides angular
    velocity data while the magnetometer and gyroscope provide spatial coordinate data which
    dictates the position of the IMU. This class is responsible for enabling or disabling
    an IMU object that has been created. Once it has been calibrated, it will continuously
    return values for the angular position and angular velocity of the IMU every two seconds.'''
    
    ## Defins IMU class
    # @Details Enables positional feedback through an IMU sensor
    
    def __init__(self,BUS_ADDR,DEV_ADDR):
        
        '''Initialization method which imports all required libraries, declares variables,
        and sets them equal to appropriate initial conditions. For readability of code, the I2C
        class of the pyb class is specifically imported. Also, the variables passed along as arguments
        are redeclared with underscores so they are not public attributes.
        @param BUS_ADDR This argument passes along the bus address of the IMU being used
        @param DEV_ADDR This argument passes along the device address of the IMU'''
        
        import pyb
        import utime
        import ustruct
        from pyb import I2C
        
        self._b_addr=BUS_ADDR
        self._d_addr=DEV_ADDR
        self._sys_cal=0
        self._gyr_cal=0
        self._acc_cal=0
        self._mag_cal=0

        
    def enable(self):
        
        '''This method enables the IMU by initializing the previously created object and setting it
        in master mode. Then it sets the IMU mode to config to clear all existing writable memory addresses.
        The utime library is called so pauses can be incorporated after each text prompt to the user.'''
        
        import utime
        from pyb import I2C
        self._i2c= I2C(self._b_addr,I2C.MASTER)
        self._i2c.mem_write(0b00000000,self._d_addr,0x3D)

        print('Inertial Measurement Unit Enabled. Calibration initializing: follow on-screen instructions.')
        utime.sleep(2)
        
    def disable(self):
        
        '''This method disables the IMU by deinitializing the previously created class object. In order to reuse
        it, the object would have to be re-created instead of just re-enabling.'''
        
        self._i2c.deinit
        print('Disabling Inertial Measurement Unit')
    
    def calib_stat(self):
        
        '''This method simply runs an infinite loop that first provides the user with calibration instructions,
        and then keeps providing the user with feedback on calibration progress until it's complete. The
        calibration vector contains the system, gyroscope, accelerometer, and magnetometer calibration values
        respectively.
        @return Cal_stat This is the returned value of the current calibration status, stating which sensors are
        currently calibrated. When all 3 sensors are calibrated, the overall system becomes calibrated'''
        
        import utime
        print('Rotate the IMU to calibrate accelerometer.')
        utime.sleep(2)
        print('Draw a figure-8 with the IMU to calibrate the magnetometer.')
        utime.sleep(2)
        print('Calibration values range between 0-3 in the order of system, gyroscope, accelerometer, and magnetometer')
        utime.sleep(2)
        print('A value of 0 indicates uncalibrated while a value of 3 indicates fully calibrated')
        utime.sleep(2)
        while self._sys_cal!=3 or self._gyr_cal!=3 or self._acc_cal!=3 or self._mag_cal!=3:
            
            self._cal_val=self._i2c.mem_read(self._b_addr,self._d_addr,0x35)
            self._sys_cal=(self._cal_val[0] >> 6) & 0b11
            self._gyr_cal=(self._cal_val[0] >> 4) & 0b11
            self._acc_cal=(self._cal_val[0] >> 2) & 0b11
            self._mag_cal=(self._cal_val[0] >> 0) & 0b11
            
            ## cal_stat stores the calibration values for the three sensors on board the IMU
            #
            #  cal_stat specifically stores the status of system calibration, gryoscope calibration, accelerometer
            #  calibration, and magnetometer calibration as values between 0-3 in a row vector. It dictates whether
            #  each sensor, and the system overall is finished calibrating. A 0 indicates an uncalibrated element while
            #  a three indicates a fully calibrated element.
            self.cal_stat=(self._sys_cal,self._gyr_cal,self._acc_cal,self._mag_cal)
            
            print(self.cal_stat)
            utime.sleep(1)
        print ('Calibration successful!')
        print ('Euler angles and angular velocities will now be shown.')
        
    def set_mode(self,mode):
        
        '''This method sets the mode of the IMU between different sensors or fusion modes that manipulate which
        sensors of the IMU are active. Most commonly and for purposes of this lab, the NDOF mode was used because
        it provided the most accurate data of the IMU's position and velocity.
        @param mode This argument passes along the desired mode to set the IMU object to, as extracted from the
        bno055 data sheet'''
        
        self._i2c.mem_write(mode, self._d_addr, 0x3D)
        
    
    def orientation(self):
        
        '''This method extracts, converts, and calculates the Euler angle values of the IMU.
        @return Euler_Angles The tuple containing the values of the euler angles with respect to the XYZ axes'''
        
        import ustruct
        self._Eu_data=self._i2c.mem_read(6,self._d_addr,0x1A)
        self._Eu_values=ustruct.unpack('<hhh',self._Eu_data)
        self._gain=(16,16,16)
        
        ## Eu_angles stores the extracted, converted and calculates values for the current IMU Euler angle positions
        #
        #  The Eu_angles object stores the result of dividing the euler values by the appropriate gain to achieve the
        #  desired unitset output. More specifically, this project uses units of degrees for angle measurements.
        #  This object is stored as a 3 value tuple.
        self.Eu_angles= tuple(ele1 // ele2 for ele1, ele2 in zip(self._Eu_values,self._gain))
        print('Angle (X,Y,Z) [deg] : '+str(self.Eu_angles))
        
        
        
        
    def velocity(self):
        
        '''This method extracts, converts, and calculates the angular velocity values of the IMU.
        @return Euler_Velocities The tuple containing the values of the angular velocities with respect to
        the XYZ axes'''
        
        import ustruct
        self._AV_data=self._i2c.mem_read(6,self._d_addr,0x14)
        self._AV_values=ustruct.unpack('<hhh',self._AV_data)
        self._gain=(16,16,16)
        
        ## AV_velocity stores the redundantly named object that contains the extracted, converted, and caclulated
        #  values of angular velocity about each coordinate axis.
        #
        #  AV_velocity is a 3-value tuple that contains angular velocity with respect to the XYZ axes in units of degrees/second
        #  This value is divided by the same gain as the Euler angles to achieve these units.
        self.AV_velocity=tuple(ele1 // ele2 for ele1,ele2 in zip(self._AV_values,self._gain))
        print('Velocity (X,Y,Z) [deg/s] : '+ str(self.AV_velocity))
        
if __name__ == '__main__':
    '''Test code for the IMU class to make sure all the methods were working properly.'''
    import pyb
    import utime
    import ustruct
    from pyb import I2C
    from IMU_Controller import IMU
    bus_addr=1
    dev_addr=40
    Imu_1= IMU(bus_addr,dev_addr)
    mode=0b00001100
    Imu_1.enable()
    Imu_1.set_mode(0b00001100)

    while True:
        Imu_1.orientation()
        Imu_1.velocity()
        utime.sleep(1)
        

