''' @file Lab0script.py'''
    
    
i = 0
while i < 1 :
    userinput = float(input ('What index number would you like to calculate? ')) # Accept user input for index value
    bra_cond=(userinput.is_integer()) # Branch condition determining whether a valid index was entered or not
    if bra_cond==True and userinput>=0:# Valid index branch
        def FibCalc(n): #Fibonacci calculator function
            '''This method calculates a Fibonacci number corresponding to a specified index.
            @param idx An integer specifying the index of the desired Fibonacci number.'''
            if n==0: # For F0
                return 0
            elif n==1: #For F1
                return 1
            else: # For specified index
                return FibCalc(n-1)+FibCalc(n-2)
        print ('Calculated Fibonacci number at specified index is: ') # General prompt
        print(FibCalc(userinput)) # Desired fibonacci number
        loop_cond = float(input('Would you like to calculate another Fibonacci number? Press 1 to continue or any other number to quit: '))
        if loop_cond==1: # Continue to loop if desired
            pass
        else: # Or exit loop and print farewell message
            print('Sequence terminated. Thanks for using the Fibonator 5000!')
            break
    else: # Branch condition for invalid index
        print('Invalid index number, try again friend.')
    
