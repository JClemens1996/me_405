'''
@file       encoder_task.py
@brief      Encoder Task 
@details    This file contains the details regarding the encoder class and its methods and functionality
            
@page       page_encoder_task Encoder Task

@brief      This file contains the details regarding the encoder class and its methods and functionality

@section    sec_encoder_task_intro Introduction
            This task reads the motor's position and computes the difference between last recorded position
            to store the change in position

@section    sec_encoder_task_fsm Finite State Machine
            This task is implemented using the following finite state machine with two tasks and
            initialization.
@subsection sec_encoder_task_trans State Transition Diagram
@image      html encoder_std.JPG "State Transition Diagram for Encoder Task"

@section    sec_encoder_task_imp Implementation
            This task is implemented by the Encoder_Task class.
'''

class Encoder_Task():
    '''
    @brief      Encoder Class
    @details    While this class has 5 methods available to the user from console, the only one that is pertinent
    to the main program for this project is the get_delta method because it not only returns the values of change in position
    and current position, but it also stores this as the current angular position of the motor in the intercommuncation variable
    theta that the controller uses for its control loop.'''
    
    ##Defines Encoder class
    
    # @Details Enables quadrature encoder readouts according to a
    # corresponding pinset and timer channel combination
    
    def __init__(self, ENC_A, ENC_B, Timer_Num):
        
        '''Creates encoder object by initializing PCB pinsets and enabling
        the proper timer in encoder counter mode.
        @param ENC_A The pyb.pin object associated with encoder phase A
        @param ENC_B The pyb.pin object associated with encoder phase B
        @param timer The pyb.pin object associated with the timer channel'''
        
        #convert class arguments to attributes
        import pyb 
        self._ENC_A=ENC_A
        self._ENC_B=ENC_B
        self._Timer_Num=Timer_Num
        
        #Initialization message
        
        print ('Encoder object is being created')
        
        #Initialize timer
        self._tim_ob=pyb.Timer(Timer_Num)
        self._tim_ob.init(period=65535,prescaler=0)
        
        #initialize timer channels
        
        self._tch1=self._tim_ob.channel(1,pyb.Timer.ENC_AB,pin=ENC_A)
        self._tch2=self._tim_ob.channel(2,pyb.Timer.ENC_AB,pin=ENC_B)
        
        #create class variables and clear them by default
        
        ## rec_position stores the recorded 16 bit unsigned number position of the motor as read by the encoder
        #
        #  rec_position is a 16 bit unsigned number because it ranges from 0-65535. This is the value used to actually
        #  calculate delta because it will account for underflow or overflow in the timer counter.
        self.rec_position=self._tim_ob.counter()
        
        ## delta stores the change in position from the last stored encoder reading in rec_position
        #
        # delta is meant to store the change in position from the encoder. This serves the dual purpose function of
        # knowing which way the motor is turning, and adding up these changes to account for the motors current position
        # at any instant
        self.delta=self._tim_ob.counter()-self.rec_position
        
        ## tot_position stores the current position of the motor as referenced by some arbitrary datum
        #
        # tot_position gets its name from the fact that it is the result of totaling up all the delta values of time. In
        # a way it measures the total change in position of the motor, but only if the reference datum is 0. Furthermore, it
        # would only measure net change in position.
        self.tot_position=0
        
        #initialization complete message
        #prompt users which pinset and which timer was initialized 
        
        print ('Encoder object has been created for the following pinset/timer combination:')
        print ('Encoder phase A: ' +str(self._ENC_A))
        print ('Encoder phase B: ' +str(self._ENC_B))
        print ('Using channels 1 & 2 of timer ' +str(self._Timer_Num))
        
        
    def update(self):
        
            '''This method updates the encoder to its new position based on the amount 
                of ticks'''
                
            #set recorded position to current encoder count in order to update
            #then print value to user for debugging purposes
            
            self.rec_position=self._tim_ob.counter()
            print ('The updated motor position is ' +str(self.rec_position))
            
    def get_position(self):
        
            '''This method returns the most recently updated position. Note:
                This may not be the physical current position'''
                
            #print the last recorded position. Weaker version of update method
            
            print ('The current motor position is ' +str(self.rec_position))
            return self.rec_position
            
    def set_position(self,new_pos):
        
            '''This method sets the position to a specified value within the 
                16 bit range of valid values. Also clears total displacement 
                (tot_position) and current displacement (delta), since a new 
                arbitrary starting position has been declared. This method 
                requires the user to input the desired position as an 
                argument.
                @param new_pos The users desired position to set the motor to'''
                
            #change position in counter and then set recorded position equal to this
            
            self._tim_ob.counter(new_pos)  
            self.rec_position=new_pos
            
            #accordingly clear delta and net displacement to reflect new origin
            #display to user the new position that they set
            
            self.delta=0
            self.tot_position=0
            
            print ('The motor position has been set to ' +str(new_pos))
            
    def get_delta(self):
        
            '''This method calculates the difference between the last recorded 
                encoder reading and the current encoder reading. It returns 
                this delta value as the difference and keeps a running total
                of net displacement
                @return delta - Change in position of the motor from the last recorded position
                @return tot_position - The current position of the model, calculated by totaling up all the delta values
                @return theta - Stores the same thing as tot_position, but serves as an intercommunication variable'''
                
            #first calculate delta as current counter minus last recorded position
            
            self.delta=self._tim_ob.counter()-self.rec_position
            
            #Test for underflow error
            
            if (self.delta>=32768):
                
            #correct underflow error if it occurs
                
                
                self.delta=self.delta-65535
                
            #test for overflow error
                
            elif (self.delta<=-32768):
                
            #correct overflow error if it occurs
                
                self.delta=self.delta+65535
                
            #display the differential displacement
                
            print('The difference in position is '+str(self.delta))
            
            #calculate net displacement
            
            self.tot_position=self.tot_position+self.delta
            self.rec_position=self._tim_ob.counter()
            
            #display net displacement
            
            print('Thus the new position is '+str(self.tot_position))
            
            ## theta stores the current angular position of the motor
            #
            # theta stores the current angular position of the motor for the other tasks to use. It's worth
            # noting that this variable actually stores the same thing as tot_position, but for ease of readability
            # and versatility as an intercommunication variable, theta is instead used.
            self.theta=self.tot_position
            
    def get_tot_pos(self):
        
            '''This method simply returns the net displacement value of the 
            motor WRT the arbitrarily declared start position. This origin 
            value is 0 be default, until manually changed by the user'''
            
            #print the last recorded net displacement value
            
            print ('The total change in position is '+str(self.tot_position))