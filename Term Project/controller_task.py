'''
@file       controller_task.py
@brief      controller Task 
@details    This file contains the details regarding the controller class and its methods and functionality
            
@page       page_controller_task controller Task

@brief      This file contains the details regarding the controller class and its methods and functionality

@section    sec_controller_task_intro Introduction
            This task is responsible for determining what duty cycle to actuate the plant (motor) at.
            This is accomplished through proportional position control with small Kp values of roughly
            0.05 - 0.1.

@section    sec_controller_task_fsm Finite State Machine
            This task is implemented using the following finite state machine with four tasks and
            initialization.
@subsection sec_controller_task_trans State Transition Diagram
@image      html pcontrol_std.JPG "State Transition Diagram for Proportional Controller Task"

@section    sec_controller_task_imp Implementation
            This task is implemented by the Controller_Task class.
'''

class Controller_Task():
    '''
    @brief      Controller Class for ME 405 term project 6/10/12
    @details    This class implements proportional position control into the ME 405 board. This is important because
    depending on the current and relative positions of the motor as gathered from the intercommunication variable theta,
    the controller task decides what actuation signal to send to the motor task'''
    
    ##Defines controller class
    
    # @Details Enables proportional control for a certain motor and encoder pair
    
    def __init__(self,motor_name,enc_name):
        
        '''Creates controller object by initializing the controller to work with a motor and encoder pair
        @param motor_name The general name of the motor as specified by the user
        @param enc_name The general name of the encoder as specified by the user'''
        
        self._motor_name=motor_name
        self._enc_name=enc_name
        trans=1
            
        
        
        
    def get_act(self):
        
        '''This method determines what the actuation singal to the motor should be based on current and relative position
        @param act_sig The actuation signal value to be sent to the motor
        @param theta The current angular position of the motor which is compared to threshold values of theta=0 units for starting
        position and theta=800 units for intermediate position'''
        
        if trans==1:
            act_sig=10
            trans=0
        if self._enc_name.theta>=800 or ts.temp>80:
            act_sig=0
        if ts.temp<80 and self._enc_name.theta>=800:
            act_sig=-11
            trans=2
        if trans==2 and self._enc_name.theta <=0:
            act_sig=0
            trans=0
        
            
            
            
        
