'''
@file       main_project.py
@brief      Main program for project
@details    This file shows how to use all the tasks and methods provided by this project
            
@page       page_main_project Temperature Sensor Task

@brief      This file contains the details regarding the Temperature  class and its methods and functionality

@section    sec_tsensor_task_intro Introduction
            This tasks reads the temperature of the DS18x20 temperature sensor and then stores it in a variable
            that flags other intercommunication booleans.

@section    sec_tsensor_task_fsm Finite State Machine
            This task is implemented using the following finite state machine with two tasks and
            initialization. 
@subsection sec_tsensor_task_trans State Transition Diagram
@image      html tsens_std.JPG "State Transition Diagram for Temperature Sensor Task"

@section    sec_tsensor_task_imp Implementation
            This task is implemented by the Tsensor_Task class.
'''
import pyb
import utime
import machine
from motor_task import Motor_Task
from encoder_task import Encoder_Task
from controller_task import Controller_Task
from tsensor_task import Tsensor_Task
#initialization (state 0) for all tasks

#Temperature sensor initialization
_ts=Tsensor_Task()

#Motor driver initialization

#Create the pin objects used for interfacing with the motor driver
pin_EN = pyb.Pin (pyb.Pin.board.PA10, pyb.Pin.OUT_PP)
# Defines enable/disable pin for user-specified channel set 
pin_IN1 = pyb.Pin (pyb.Pin.board.PB4, pyb.Pin.OUT_PP)
#Define CPU pin for IN_1 of specified channel 
pin_IN2 = pyb.Pin (pyb.Pin.board.PB5, pyb.Pin.OUT_PP)
#Define CPU pin for IN_2 of specified channel 

#Create the timer object used for PWM generation
tim = pyb.Timer(3, freq=20000)
#Enable timer 3 with a frequency of 20000 Hz

#Create a motor object passing in the pins and timer
moe = Motor_Task(pin_EN, pin_IN1, pin_IN2, tim)

#Enable the motor driver
moe.enable()

#Set the duty cycle to 10 percent
moe.set_duty(0)

#Encoder initialization

#The code below is meant to write a test program for the encoder class. It
#only executes if the script is executed as a standalone program.  

#create object for encoder 1
        
ENC_1 = Encoder_Task(pyb.Pin.board.PB6,pyb.Pin.board.PB7,4)

#create object for encoder 2

ENC_2 = Encoder_Task(pyb.Pin.board.PC6,pyb.Pin.board.PC7,8) 
    
#show users current encoder origin for reference
    
ENC_1.get_position()
ENC_2.get_position()

#p-control initialization

pc1=Controller_Task.(moe,ENC_1)



# main loop
while True:
    
    #get current motor position
    ENC_1.get_delta()
    
    #use current motor position and relative motor position along with
    #probe sensor temperature to determine actuation signal from controller
    pc1.det_act()
    
    #set motor duty cycle according to controller's actuation signal
    moe.set_duty(act_sig)
    
    #update sensor temperature to see if it meets or exceeds the threshold value of 80 degC
    _ts.get_temp()
    
    #sleep the program to prevent UI clutter. This also accounts for the frequency of the
    #cooperative multitasking
    utime.sleep_ms(50)
    
