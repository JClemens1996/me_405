'''
@file       tsensor_task.py
@brief      Temperature Sensor Task 
@details    This file contains the details regarding the temperature sensor class and its methods and functionality
            
@page       page_tsensor_task Temperature Sensor Task

@brief      This file contains the details regarding the Temperature  class and its methods and functionality

@section    sec_tsensor_task_intro Introduction
            This tasks reads the temperature of the DS18x20 temperature sensor and then stores it in a variable
            that flags other intercommunication booleans.

@section    sec_tsensor_task_fsm Finite State Machine
            This task is implemented using the following finite state machine with two tasks and
            initialization. 
@subsection sec_tsensor_task_trans State Transition Diagram
@image      html tsens_std.JPG "State Transition Diagram for Temperature Sensor Task"

@section    sec_tsensor_task_imp Implementation
            This task is implemented by the Tsensor_Task class.
'''

import time
import machine
import onewire
import ds18x20

class Tsensor_Task:
    '''
    @brief      Temperature Sensor Class
    @details    This class defines the temperature sensor task and is responsible for initializing the temperature
    sensor which entails enabling the GPIO pin, scanning for the device, and then creating an object out of that
    byte array. This was made possible through modules contributed by Damien George and other non-profit contributors
    whose work can be found at https://github.com/dpgeorge/micropython/tree/master/drivers/onewire
    This repository contains two modules, one for the onewire library and the other for the ds18x20 temperature sensor
    library. The onewire library allows bits or bytes to be read or written to the memory address of any device scanned
    on that bus. This is helpful for things such as changing the significant figures on the readout of a sensor, or adjusting
    parameters associated with each sensor on the same bus separately.'''
    


    
    def __init__ (self):
        
        '''Initialization for the temperature sensor includes importing the externally burrowed github libraries
         including the onewire, machine, and ds18x20 modules. Beyond that, a machine pin is created on the GPIO pin
         connected to the probe sensor and then the sensor object is created from that pin object. Finally,
         a device scan reveals all the sensors of that type on the bus and the resulting byte arrays are printed'''

        import time
        import machine
        import onewire
        import ds18x20
        # the device is on GPIO12
        dat = machine.Pin('PB10')

        # create the onewire object
        
        ## tsens1 is the object associated with the first temperature sensor on the communication bus.
        #
        # tsens1 stores the object for the first temperature sensor acquired by the bus scan. If there are multiple
        # sensors to be used, then additional objects must be created to store these. For purposes of this project,
        # tsens1 is defined as the first and single DS18x20 waterproof external temperature sensor probe being used
        # to determine liquid temperature.
        self.tsens1 = ds18x20.DS18X20(onewire.OneWire(dat))

        # scan for devices on the bus
        
        ## roms stores the byte arrays found by the bus scan.
        #
        # roms stores the byte arrays found by the bus scan. If there are multiple sensors being used, then the byte
        # array desired must be specifically picked out of the scan list so it can be unpacked into constituent values
        # including the CRC code, the  serial number, family code, and actual temperature value recorded by the sensor.
        self.roms = self.tsens1.scan()
        print('Found Devices - [CRC Code/Serial Number/Family Code] -> [8bit/48bit/8bit identifier]:', self.roms)

    def get_temp (self):
        
        '''This method is responsible for reading the temperature from the sensor a single time, printing that
        value, and then storing in the variable temp. It's worth noting that this is generally written to print the
        temperatures of all sensors on the bus, but for the scope of this project that is just the single probe so only
        one value is returned therefore only the single variable is needed to store it.
        @return temp is returned as the variable storing the last recorded temperature of the sensor (i.e. the tea)'''
        
        import time
        # loop 10 times and print all temperatures
        for i in range(1):
            print('Current temperature(degC): ', end=' ')
            self.ds.convert_temp()
            time.sleep_ms(1)
            for rom in self.roms:
                print(self.ds.read_temp(rom), end=' ')
            print()
            ## temp simply stores the recorded value of temperature as determined by the read_temp method
            #
            # temp is a variable definition intended to physically store the last known temperature reading acquired
            # by the sensor. This is necessary because it is used as an intercommunication variable between the
            # temperature sensor and the controller task. When the temperature of the liquid drops below the threshold value
            # of 75 degC, then a super-boolean variable labeled trans is set to 2 and then the controller knows what actuation
            # signal to send the motor.
            self.temp=self.ds.read_temp(rom)