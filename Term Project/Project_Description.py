## @page TPD&D Term Project Description & Details
#
# @section O Overview
# The scope of this project was aimed towards developing a tea diffusing device that steeped for the correct amount of time
# autonomously. This depended on variables such as how strong the tea leaves were, what type of tea was being brewed, and what volume
# of tea was being steeped. With these input parameters, the program is capable of determining how long the diffuser needs to be submerged in
# the pre-heated water, and then actuating both the submerging and removal of the teabag from the mug. This was accomplished through use of
# the DS18B20 external waterproof temperature sensor and a simple heat transfer analysis to correlate time steeped to net heat transfer
# and rate of heat transfer (Qnet and Qdot). Other features include the ability to ensure that no fluid drips outside the mug after the
# tea diffuser has been removed and while the project timeline did not allow for implementation of the following feature, it was also
# intended to have an IMU stabilizer that sealed the mug if the stand began to tilt beyond a certain threshold. This simply served as a failsafe
# mechanism to make sure that none of the electronic components were exposed to liquid in the event the tower tipped, and to also protect the
# user from hot liquid spilling.
#
# @section DD Design Description
#
# The design for this project consists of the DS18B20 temperature sensor which serves as a trigger for when the program should switch between
# three different operating modes. The first operating mode is standby where the diffuser is suspended above the mug and not submerged in the fluid.
# The second operating mode is described as the steeping mode where the temperature sensor begins acquiring data and comparing it to a target transition
# value. During this mode, the motor is at a new position where the diffuser is now suspended in the liquid and diffusing begins. This means the third
# operating mode is the transition betwen state one and two, where the motor is being actuated by a closed loop proportional controller which tells it what
# new position it needs to arrive at in order to initiate the next mode. Note also how the program is intended to begin in standby mode (which can be forced
# through a soft-reset on the main PCB) and then ends in standby mode for another use after all three states have subsequently occured. 
#
# @section CAD CAD Model
#
# Shown below are CAD model renderings of what the original design was intended to look like. Key features are noted through description arrows. It is also
# worth noting that this was a preliminary design and the actual prototype, which is discussed in the next section, has some significant differences either due
# to ease of manufacturing, connection and cable management, user practicality, and design viability.
# @image html cad_front.JPG "Front view of CAD model"
# @image html cad_side.JPG "Side view of CAD model"
# @image html cad_iso.JPG "Isometric view of CAD model"
#
# @section MP Model Prototype
#
# This page details the physical prototype that was manufactured. Distinctions between the prototype and CAD model are noted. Furthermore, and brief video
# demonstrating a single use is shown in a video at the bottom of this page.
# @image html proto_front.jpg "Front view of model prototype"
# @image html proto_back.jpg "Back view of model prototype"
# @image html proto_iso.jpg "Isometric view of model prototype"
#
# @section MS Multitasking Structure
#
# This program incorporates cooperative multitasking by having a main program that continuously checks the state of each driver. There are three main drivers used
# in this program. The first is the temperature sensor driver which continuously checks the temperature, records it, and compares it to separate
# threshold values that indicate the time has occured to switch states in other drivers. This is accomplished through the user of intercommunication variables that
# the temperature sensor driver sets and the other drivers clear after successful use. This leads into the next driver which is the proportional control motor driver.
# This driver is somewhat a combination of two separate, but integrated drivers consisting of a proportional controller and a motor actuator. Therefore, the p-control
# motor driver is responsible for actuating the motor in closed loop proportional control by reading the error as the difference between actual position and desired
# position and then acuating the motor based on a proportionally scaled value of this error. The only other driver present in this system is the encoder driver which
# is responsible for determining and recording the current position of the motor through the use of a built-in optical encoder.
#
# @section T Tasks
#
# All of the tasks and their corresponding state transition diagrams are shown on the @subpage page_tasks "Task List" page.
# There are four tasks in total which are the temperature sensor task, motor driver task, encoder driver task, and proportional controller task.
# Shown below are the state-transition diagrams for each of the tasks in this program. There are four tasks that the main program cooperatively multitasks which are
# the temperature driver, the motor driver, the encoder driver, and the proportional controller.
#
# @section CLC Closed-loop control
#
# Closed-loop control is present in this design for two reasons, one less advanced than the other. The first is through the motor driver which is actuated based on
# closed-loop proportional control. More convincingly, the motors torque and position are changed based on how close the tea is done to steeping. As it nears the target
# threshold value, the motors' position is changed to slowly remove the diffuser from the liquid, and as this happens the buoyant force on the diffuser decreases which
# results in an increased torque on the motor output shaft. This means it must apply a larger torque in order to change the position by the same increments. This is
# also accomplished through closed-loop control where the actuation signal is a function of current position, desired position, and motor output duty cycle.
#
# @section SandA Sensors & Actuators
#
# The sensors and actuators present in this system include the temperature sensor and motor encoder. The temperature sensor reads the liquid temperature to determine
# when it is time to transition between states, and the encoder is a sensor that measures the motor position so it knows when to clear the intercommuncation variable
# that the temperature sensor sets. The motor is actuated by the measurements from these sensors. Much of the functionality for the
# temperature sensor is derived from the github library found at https://github.com/dpgeorge/micropython/tree/master/drivers/onewire
# This library contained the onewire and DS18x20 modules. Credit goes to Damien George and other contributors for making this publicly
# available. More details about these two modules are described in the temperature task.
#
# @section Video Demonstration
# @image html demo.gif "Single use operation. Note how the closed-loop control is at 10x scaled speed to show the typical start-end operation"
#
# @section EU Example Usage
#
# The following link: https://bitbucket.org/CuriosityJPL/me_405/src/master/Term%20Project/main_project.py refers to the main program associated with using this program. The only things the user is required to do is
# to define the objects for the motor, encoder, and temperature sensor (considering they can use any timer channel/pinset
# combination for the motor and encoder and there can be multiple temperature sensors on the same onewire bus). Physically, all
# that must be done beyond that is to place the mug on the stand, fill the diffuser with tea, and then start the program with a
# soft reset on the board.
#
# @image html main_photo.JPG 


