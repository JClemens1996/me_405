'''
@file       motor_task.py
@brief      Motor Task 
@details    This file contains the details regarding the motor class and its methods and functionality
            
@page       page_motor_task Motor Task

@brief      This file contains the details regarding the motor class and its methods and functionality

@section    sec_motor_task_intro Introduction
            This task is responsible for simply actuating the motor through PWM on a corresponding
            timer channel and pinset combination.

@section    sec_motor_task_fsm Finite State Machine
            This task is implemented using the following finite state machine with three tasks and
            initialization.
@subsection sec_motor_task_trans State Transition Diagram
@image      html motor_std.JPG "State Transition Diagram for Motor Task"

@section    sec_motor_task_imp Implementation
            This task is implemented by the Motor_Task class.
'''
import pyb
import utime

class Motor_Task():
    '''
    @brief      Motor Class
    @details    This class defines a dummy task to use as an example. The task
                simply prints a timestamp to the terminal every 10 second.'''
    ''' This class implements a motor driver for the ME405 board. This will be used to drive the motor based on an actuation
    signal which is determined through a proportional controller meant for motor position control. The only job for this class
    is to make the motor driver object based on a user passing in a pinset and timer combination, and then set the duty cycle
    as necessary to ensure proper position control. Enable and disable features are included as a failsafe in case position
    control encounters errors.'''
    
    ## Defines MotorDriver class
    
    # @Details Enables a PWM driven method that operates on Timer 3 using 
    # channels 1 and 2 corresponding to pins B4 and B5 of the motor board
    
    def __init__ (self,EN_pin,IN1_pin,IN2_pin,timer):
        '''Creates a motor driver by initializing GPIO pins and turning
        the motor off for safety.
        @param EN_pin   A pyb.Pin object to use as the enable pin
        @param IN1_pin  A pyb.Pin object to use as the input to half bridge 1
        @param IN2_pin  A pyb.Pin object to use as the input to half bridge 2
        @param timer    A pyb.pin object to use for pulse-width modulation 
        generation on IN1_pin and IN2_pin'''
        import pyb
        self._EN_pin=EN_pin
        self._IN1_pin=IN1_pin
        self._IN2_pin=IN2_pin
        self._timer=timer
        
        print ('Motor driver is being created')
        self._EN_pin.low()
        # set en/d pin low to make sure motor is off at program startup
        
        self._t3ch1 = timer.channel (1, pyb.Timer.PWM, pin = IN1_pin)
        #Assign channel 1 of timer 'n'
        self._t3ch2=timer.channel(2, pyb.Timer.PWM, pin=IN2_pin)
        #Assign channel 2 of timer 'n'
        
    def enable (self): #The enable method of MotorDriver class
        '''This method simply enables the motor by toggling high the EN pin of the motor object. This determines whether
        PWM occurs within the two other pins that determine motor duty cycle.'''
        print ('Enabling motor') #Debugging prompt
        self._EN_pin.high() #Set en/d pin high to enable motor
        
    def disable (self): #The disable method of MotorDriver class
        '''Just as the enable method toggles the EN pin high, the disable method toggles the EN pin low. If this pin is toggled
        low, then PWM will not occur in the other two timer channel pins of the motor driver object'''
        print ('Disabling motor') #Debugging prompt
        self._EN_pin.low() #Set en/d pin to low to disable motor
        
    def set_duty (self, duty): #The duty cycle set method within MotorDriver class
        '''This method sets the duty cycle to be sent to the motor to the 
        given level. Positive values cause an effort in one direction, 
        negative values in the opposite direction
        @param duty A signed integer holding the duty cycle of the PWM signal
        sent to the motor'''
        if duty<=0: #For negative values of duty cycle
            self._t3ch1.pulse_width_percent(0) #Clear channel 1 PWM
            self._t3ch2.pulse_width_percent(abs(duty)) #Set channel 2 PWM to absolute value of duty input
            self._EN_pin.high() #Make sure motor is enabled
        else: #For positive values of duty cycle
            self._t3ch1.pulse_width_percent(duty) #Set channel 1 PWM to value of duty input
            self._t3ch2.pulse_width_percent(0) #Clear channel 2 PWM
            self._EN_pin.high() #Make sure motor is enabled
        print ('Duty cycle is now ' + str(duty)) #Print new duty cycle value to user
        
if __name__== '__main__':
    #The code below is meant to write a test program for the motor class. It
    #only executes if the script is executed as a standalone program.
    
    #Create the pin objects used for interfacing with the motor driver
    pin_EN = pyb.Pin (pyb.Pin.board.PA10, pyb.Pin.OUT_PP)
        # Defines enable/disable pin for user-specified channel set 
    pin_IN1 = pyb.Pin (pyb.Pin.board.PB4, pyb.Pin.OUT_PP)
        #Define CPU pin for IN_1 of specified channel 
    pin_IN2 = pyb.Pin (pyb.Pin.board.PB5, pyb.Pin.OUT_PP)
        #Define CPU pin for IN_2 of specified channel 
    
    #Create the timer object used for PWM generation
    tim = pyb.Timer(3, freq=20000)
        #Enable timer 3 with a frequency of 20000 Hz
    
    #Create a motor object passing in the pins and timer
    moe = Motor_Task(pin_EN, pin_IN1, pin_IN2, tim)
    
    #Enable the motor driver
    moe.enable()
    
    #Set the duty cycle to 10 percent
    moe.set_duty(0)

    
